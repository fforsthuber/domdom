<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/", name="index", methods={"GET"})
 */
class IndexController
{
    public function __invoke(Request $request): Response
    {
        return new Response('<h1>Hello DomDom</h1>');
    }
}
